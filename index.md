---
layout: default
title: Home
nav_order: 1
description: "A PoC for Courseware as Code 2021 GsoC proposal, using UBA Computer Architecture and Organization as an example."
permalink: /
---

# Computer Organization and Architecture

Welcome to the Computer Organization and Architecture course!
