---
title: 
- Introduction to the RISC-V Instruction Set Architecture
author:
- Alejandro Rusi
theme:
- Copenhagen
institute:
- GsoC
---

# RISC-V

![Risc-V Logo](assets/images/logo.png)

RISC-V is an open standard instruction set architecture (ISA) based on established reduced instruction set computer (RISC) principles. Unlike most other ISA designs, the RISC-V ISA is provided under open source licenses that do not require fees to use.
RISC-V is:

- Open Standard and Open Source
- Modular
- Extendible

# Format

![ISA Format](assets/images/format.png)

# Extensions

## Multiplication and Division

Allows for the usage of **multiplication** and **division** operations. Assumes that there are registers with 64-bits.

## Atomic Instructions

Consistent access to memory.

## Bit Manipulation

Still unapproved, but will aid support for common cryptographic, graphic and mathematical bit operations.
