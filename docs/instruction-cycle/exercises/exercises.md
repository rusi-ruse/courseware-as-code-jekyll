---
layout: default
title: Exercises
parent: Instruction Cycle
nav_order: 2
---

# Exercises
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

### Computer Architecture Exercises

[Exercises]({{ site.url }}/{{ site.baseurl }}/assets/pdf/practica3.pdf).