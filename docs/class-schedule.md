---
layout: default
title: Class Schedule
nav_order: 2
---

# Class Schedule

* Historic Evolution of Computers
* Von Neumann Machines
* Information representation
* Digital Logic
* Computer Architecture
* Introduction to MicroArchitecture
* Memory
* Input / Output
* Interruptions
* Data Buses
